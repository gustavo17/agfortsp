#pragma once
#include <optional>
#include <random>
#include <vector>
#include "tour.h"

extern const int HILOS;
extern const int ITERACIONES;
extern const int PROBMUT;
extern const double MORTALIDAD_INFANTIL;
extern const int SELECCIONADOS;
extern const int UMBRAL_MAX_AFINIDAD;
extern const int UMBRAL_MEDIA_AFINIDAD;


class Poblacion {
    public:
      Poblacion(int n, const Mapa &m);
      std::pair<Tour*, Tour*> elegirPadres(std::mt19937 & generador);
      Tour crearIndividuo(const Mapa &m, std::mt19937 &engine);
      void reporte();
      void nuevaGeneracion(const Mapa &m);
      std::optional<Tour> &getMejorSolucion();
      int mejorValor;
      int peorValor;
      double promedio;
      void estadisticas();

    private:
        std::vector<std::optional<Tour>> soluciones;
        
};