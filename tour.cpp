#include "tour.h"
#include "poblacion.h"
#include <limits>
#include <optional>
#include <queue>
#include <random>
#include <vector>
#include <iostream>
#include <cassert>
#include <format>

Tour::Tour(const Mapa &m, std::mt19937 &engine):ady(m.data.size()) {
    largo=m.data.size();
    std::uniform_int_distribution<int> dist(0,largo-1);
    std::vector<bool> visitados (largo,false);
    int inicial=dist(engine);
    int actual=inicial;
    int cont=0;
    while (cont++<largo){
        ady[actual].id=actual;
        visitados[actual]=true;

        int siguiente=inicial;
        if (cont<largo){
            std::vector<int> candidatos_disponibles;
            auto & candidatos=m.candidatosDeNodo[actual];
            for (auto c:candidatos){
                if (!visitados[c]) candidatos_disponibles.push_back(c);
            }
            if (candidatos_disponibles.size()>0){       //seleccion por candidatos
                std::uniform_int_distribution<int> dist2(0,candidatos_disponibles.size()-1);
                siguiente=candidatos_disponibles[dist2(engine)];
            } else {                                    //seleccion al azar
                int posible=dist(engine);
                while (visitados[posible]){
                    posible=(posible+1)%largo;
                }
                siguiente=posible;
            } 
        }
        ady[actual].siguiente=&ady[siguiente];
        ady[siguiente].anterior=&ady[actual];
        actual=siguiente;
    }
    
    //actualiza posiciones
    Nodo *inicio=&ady[0];
    inicio->posicion=0;
    Nodo *ptr_actual=inicio->siguiente;
    int pos=1;
    while (ptr_actual!=inicio){
        ptr_actual->posicion=pos++;
        ptr_actual=ptr_actual->siguiente;
    }

}

void Tour::mostrar() const {
    std::cout<<"0,";
    const Nodo * inicio=&ady[0];
    const Nodo * actual=inicio->siguiente;
    while (actual!=inicio){
        std::cout<<actual->id<<",";
        actual=actual->siguiente;
    }
    std::cout<<"0"<<std::endl;
}

int Tour::evaluar(const Mapa &m)  {
    Nodo *inicio=&ady[0];
    Nodo *ptr=inicio->siguiente;
    int suma=0;
    while(ptr!=inicio) {
        suma+=m.data[ptr->anterior->id][ptr->id];
        ptr=ptr->siguiente;
    }
    suma+=m.data[ptr->anterior->id][ptr->id];
    return suma;
}


int Tour::twoOpt(std::mt19937 &engine, const Mapa &m, const std::optional<Tour> & mejor) {
    std::uniform_int_distribution<int> aleatorio(0,largo-1);
    Nodo * inicio=&ady[aleatorio(engine)];
    Nodo *t0=inicio;
    Nodo *t1;
    
    while (true){
        t1=t0->siguiente;
        if (!mejor.has_value()) break;
        if (mejor->ady[t0->id].siguiente->id!=t1->id && mejor->ady[t0->id].anterior->id!=t1->id) break;
        t0=t0->siguiente;
        if (t0==inicio) return 0; // sale al dar la vuelta completa (llegar a inicio de nuevo)
    }
    

    for (int nodo: m.candidatosDeNodo[t1->id]){
        Nodo *t2=&ady[nodo];
        if (t1->siguiente==t2 || t0==t2) continue; //no elige los que ya estan en el tour
        int g0=m.data[t0->id][t1->id]-m.data[t1->id][t2->id];
        if (g0<=0) continue;
        Nodo *t3=t2->anterior;
        int ganancia=g0+m.data[t2->id][t3->id]-m.data[t3->id][t0->id];
        if (ganancia>0){
            int t3t1=t3->posicion-t1->posicion; if (t3t1<0) t3t1+=ady.size();
            int t0t2=t0->posicion-t2->posicion; if (t0t2<0) t0t2+=ady.size();
            if (t3t1<t0t2)
                mover(t0, t1, t2, t3);
            else 
                mover(t3, t2, t1, t0);
            return ganancia;
        } else {
            //3-opt
            for (auto ct4:m.candidatosDeNodo[t3->id]){
                Nodo *t4=&ady[ct4];
                int G1=g0+m.data[t2->id][t3->id]-m.data[t3->id][t4->id];
                if (G1<=0 || t4==t3->siguiente || t4==t3->anterior)continue;
                bool post4=ENTRE(t1,t3,t4);
                Nodo* t5=post4?t4->siguiente:t4->anterior;
                int g2=G1+m.data[t4->id][t5->id]-m.data[t5->id][t0->id];
                if (g2>0){
                    mover(t0,t1, t2, t3);
                    mover(t0,t3, t4, t5);
                    return g2;
                }
            }
        }
    }
    return 0; 
}

void Tour::mover(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3) {
    int pos=t3->posicion;
    Nodo *ptr=t1;
    while (ptr!=t2){
        ptr->posicion=pos--;
        if (pos<0)pos=largo-1;
        std::swap(ptr->siguiente, ptr->anterior);
        ptr=ptr->anterior;
    }
    t0->siguiente=t3; t3->anterior=t0;
    t1->siguiente=t2; t2->anterior=t1;
}

bool Tour::isConexa() const {
    //es mas eficiente que bool (por empaquetado)
    const Nodo *inicio=&ady[0];
    Nodo * actual=inicio->siguiente;
    int cont=1;
    int pos=inicio->posicion;
    pos++;if (pos==ady.size())pos=0;
    while (actual!=inicio){
        if (pos!=actual->posicion) {
            std::cout<<"esperada : "<<pos<<", encontrada :"<<actual->posicion<< ", en nodo "<<actual->id<<std::endl;
            return false;
        }
        pos++;if (pos==ady.size())pos=0;
        cont++;
        if (actual->siguiente->anterior!=actual){
            std::cout<<"anterior mal configurado en nodo: "<<actual->siguiente->id<<std::endl;
            return false;
        }
        actual=actual->siguiente;
        if (cont>ady.size()) return false;
    }
    return cont==ady.size();
}
Tour::Tour(std::vector<int> ruta):largo(ruta.size()-1),ady(ruta.size()-1) {
    largo=ady.size();
    auto anterior=ruta.begin();
    int posicion=0;
    auto inicio=ruta.begin()+1;
    for (auto it=inicio;it!=ruta.end();it++){
        posicion++;if (posicion==largo)posicion=0;
        Nodo *ptr=&ady[*it];
        ptr->posicion=posicion;
        ptr->id=*it;
        ptr->anterior=&ady[*anterior];
        if (it==ruta.end()-1){
            ptr->siguiente=&ady[*inicio];
        } else {
            ptr->siguiente=&ady[*(it+1)];
        }
        anterior=it;
    }
}
Tour::Tour(const Tour &sol):ady(sol.ady.size()),largo{sol.largo}{
   //std::cout<<"copiando tour"<<std::endl;
    for (int i=0;i<ady.size();i++){
        ady[i].id=sol.ady[i].id;
        ady[i].posicion=sol.ady[i].posicion;
        ady[i].siguiente=&ady[sol.ady[i].siguiente->id];
        ady[i].anterior=&ady[sol.ady[i].anterior->id];
    }
    costo=sol.costo;
}

Tour::Tour(Tour &&sol):ady{std::move(sol.ady)},costo{sol.costo},largo{sol.largo} {
    //std::cout<<"moviendo tour"<<std::endl;
}

Tour &Tour::operator=(Tour &&sol) {
    //std::cout<<"asignacion de movimiento tour"<<std::endl;
    ady=std::move(sol.ady);
    costo=sol.costo;
    largo=sol.largo;
    return *this;
}
bool Tour::operator<(const Tour &otro) const { return costo < otro.costo; }

Tour::Tour(const Mapa &m, const Tour &padre, const Tour &madre,std::mt19937 &engine,const std::optional<Tour> &mejor):ady(padre.ady.size()) {
    largo=padre.largo;
    std::uniform_int_distribution<int> dist(0,largo-1);
    int corte1=dist(engine); const Nodo *ptr_p0=&padre.ady[corte1]; //t1 es to->siguiente
    int corte2=corte1;
    while (corte2==corte1) corte2=dist(engine); 
    const Nodo *ptr_p3=&padre.ady[corte2]; //t2 es t3->anterior
    
    //computa el recorrido mas corto (si parte de p0 o de p3 hacia el siguiente)
    int distanciaEntreCortes=ptr_p3->posicion-ptr_p0->posicion;if (distanciaEntreCortes<0) distanciaEntreCortes+=largo;
    if (distanciaEntreCortes>=largo/2) std::swap(ptr_p0,ptr_p3); //elige el recorrido mas corto

    //para calcular el posicionamiento del hijo
    int offset=padre.ady[0].posicion;

    //inicializa aux para recorrido y copia
    const Nodo *aux=ptr_p0;
    int pos=aux->posicion-offset; if (pos<0)pos+=largo;
    int nPos=pos;

    //recorre la madre hasta posicion de inicio de corte
    const Nodo *ptr_m0=&madre.ady[0];
    if (nPos<=(largo/2)) {
        while ((nPos--)>0) ptr_m0=ptr_m0->siguiente;
    } else {
        nPos=largo-nPos;
        while ((nPos--)>0) ptr_m0=ptr_m0->anterior;
    }

    //inicializa los reemplazos
    std::vector<int> reemplazos(largo, -1);
    const Nodo *aux_m=ptr_m0;

    //copia el padre al hijo
    while (aux!=ptr_p3->siguiente){
        Nodo *nodoHijo=&ady[aux->id];
        nodoHijo->id=aux->id;
        nodoHijo->posicion=pos++; if (pos==largo)pos=0;
        nodoHijo->siguiente=&ady[aux->siguiente->id];
        nodoHijo->anterior=&ady[aux->anterior->id];
        if (aux->id!=aux_m->id)reemplazos[aux->id]=aux_m->id; //setea el reemplazo
        aux=aux->siguiente;
        aux_m=aux_m->siguiente;
    }

    //copia la madre al hijo
    Nodo *anterior=&ady[ptr_p3->id];
    while (aux_m!=ptr_m0) {
        int id=getIdReemplazo(reemplazos, aux_m->id);
        Nodo *nodoHijo=&ady[id];
        nodoHijo->id=id;
        nodoHijo->posicion=pos++; if (pos==largo)pos=0;
        nodoHijo->anterior=anterior;
        nodoHijo->anterior->siguiente=nodoHijo;
        anterior=nodoHijo;
        aux_m=aux_m->siguiente;
    }

    ady[ptr_p0->id].anterior=&ady[getIdReemplazo(reemplazos,ptr_m0->anterior->id)];
    ady[ptr_p0->id].anterior->siguiente=&ady[ptr_p0->id];

    //FIN PMX

    //aplica busqueda en el hijo
    int nSinMejora=0;
    int MAX=(ady.size()<<1);
    while (nSinMejora<MAX){
        int ganancia=twoOpt(engine, m, mejor);
        if (ganancia>0){
            nSinMejora=0;
            costo-=ganancia;
        } else {
            nSinMejora++;
        }
    }
}

int Tour::getIdReemplazo(const std::vector<int> &reemplazos, int id) {
    while (reemplazos[id]!=-1)id=reemplazos[id];
    return id;
}

//aplica la mejor ganancia encontrada sea o no positiva
int Tour::mutar(std::mt19937 &engine, const Mapa &m) {
    int sCand=m.candidatosDeNodo[0].size();
    std::uniform_int_distribution<int> aleatorio(0,largo-1);
    
    Nodo * inicio=&ady[aleatorio(engine)];
    Nodo *t0=inicio;
    Nodo *t1=t0->siguiente;

    std::vector<std::pair<int,Nodo *>> candidato;
    for (int cand:m.candidatosDeNodo[t1->id]){
        Nodo *t2=&ady[cand];
        if (t1->siguiente==t2 || t0==t2) continue; //no elige los que ya estan en el tour
        int g0=m.data[t0->id][t1->id]-m.data[t1->id][t2->id];
        if (g0>0){
            candidato.push_back({g0,t2});
        }
    }
    if (candidato.size()==0) return 0;
    std::uniform_int_distribution<int> aleatorio2(0,candidato.size()-1);
    auto &elegido=candidato[aleatorio2(engine)];
    int g0=elegido.first;
    Nodo *t2=elegido.second;

    Nodo *t3=t2->anterior;
    int ganancia=g0+m.data[t2->id][t3->id]-m.data[t3->id][t0->id];        
    int t3t1=t3->posicion-t1->posicion; if (t3t1<0) t3t1+=ady.size();
    int t0t2=t0->posicion-t2->posicion; if (t0t2<0) t0t2+=ady.size();
    if (t3t1<t0t2){
        mover(t0, t1, t2, t3);
    } else {
        mover(t3, t2, t1, t0);
    }
    return ganancia;
}
bool Tour::operator>(const Tour &otro) const {return costo>otro.costo;}

int Tour::getCompatibilidad(std::mt19937 &engine,Tour *otro) {
    std::uniform_int_distribution<int> dist(0,largo-1);
    int suma=0;
    for (int i=0;i<CARACTERISTICAS_AFINIDAD;i++){
        int nodo=dist(engine);
        if (ady[nodo].siguiente->id!=otro->ady[nodo].siguiente->id && ady[nodo].siguiente->id!=otro->ady[nodo].anterior->id) suma++;
        if (ady[nodo].anterior->id!=otro->ady[nodo].siguiente->id && ady[nodo].anterior->id!=otro->ady[nodo].anterior->id) suma++;
    }
    return suma;
}
