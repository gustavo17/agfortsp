#pragma once
#include "mapa.h"
#include "nodo.h"
#include <random>
#include <vector>

#define ENTRE(menor, mayor, entre) \
    ((menor->posicion <= entre->posicion && entre->posicion <= mayor->posicion) || \
     ((mayor->posicion < menor->posicion) && ((menor->posicion <= entre->posicion) || (entre->posicion <= mayor->posicion))))

extern const int CARACTERISTICAS_AFINIDAD;

class Tour {
    public:
      Tour(std::vector<int> ruta);
      Tour(const Mapa &m,std::mt19937 & engine);

      Tour(const Tour &sol);
      Tour(Tour &&sol);
      Tour &operator=(Tour &&sol);

      Tour(const Mapa &m,const Tour &padre, const Tour &madre,std::mt19937 &engine,const std::optional<Tour> &mejor);

      void mostrar() const;
      int evaluar(const Mapa &m) ;
      int twoOpt(std::mt19937 &engine, const Mapa &m, const std::optional<Tour> & mejor);
      void mover(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3);
      bool isConexa() const;
      int mutar(std::mt19937 &engine, const Mapa &m);
      int getCompatibilidad(std::mt19937 &engine,Tour *otro);
      bool operator<(const Tour &otro) const;
      bool operator>(const Tour &otro) const;

      int costo;
      int largo;
    private:
      int getIdReemplazo(const std::vector<int> & reemplazos, int id);
      std::vector<Nodo> ady;
};