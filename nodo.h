#pragma once

struct Nodo {
    Nodo *siguiente; 
    Nodo *anterior;
    int posicion;
    int id;
};