#include "poblacion.h"
#include "mapa.h"
#include <iostream>
#include <optional>

const int HILOS=6;
const int ITERACIONES=3;                //CREACION POB INICIAL
const int PROBMUT=10;                   //PROBABILIDAD DE MUTACION
const double MORTALIDAD_INFANTIL=0.02;  //PEORES HIJOS ELIMINADOS EN CADA GENERACION
const int SELECCIONADOS=6;              //TAMANO TORNEO DE SELECCION
const int MAXSINMEJORA=35;              //GENERACIONES SIN MEJORA
const int TAMANOPOBLACION=1000;
const int ITER=3;                      //REINICIOS DE POBLACION
const int CARACTERISTICAS_AFINIDAD=10; 
const int UMBRAL_MAX_AFINIDAD=8; 
const int UMBRAL_MEDIA_AFINIDAD=5; 


int main() {
    Mapa m {"../att532.dat"};
    m.computarCandidatos(25);

    int sumaEstadistica=0;
    int optimoCount=0;

    auto inicio = std::chrono::high_resolution_clock::now();
    int it=0;
    std::optional<Tour> mejorEver;
    while (it<ITER){
        Poblacion p {TAMANOPOBLACION,m};
        int mejor=p.mejorValor;
        int cont=0,i=0;
        std::cout<<i++<<"\tBEST: "<<((mejorEver.has_value())?mejorEver->costo:mejor)<<", ";
        p.reporte();
        while(cont<MAXSINMEJORA){
            if (i % 10==0){
                int valMejor=(!mejorEver.has_value() || mejorEver->costo>mejor)?mejor:mejorEver->costo;
                std::cout<<i++<<"\tBEST: "<<valMejor<<", ";
                p.reporte();

            }
            p.nuevaGeneracion(m);
            if (p.mejorValor<mejor){
                mejor=p.mejorValor;
                int valMejor=(!mejorEver.has_value() || mejorEver->costo>mejor)?mejor:mejorEver->costo;
                if (mejor<mejorEver->costo)
                    std::cout<<i<<"\033[31m\tBEST: "<<valMejor<<", ";
                else 
                    std::cout<<i++<<"\tBEST: "<<valMejor<<", ";
                
                    
                p.reporte();
                std::cout<<"\033[0m";
                cont=0;
            } else {
                cont++;
            }
            i++;
        }
        
        if (!mejorEver.has_value() || p.getMejorSolucion()->costo<mejorEver->costo)mejorEver=std::move(p.getMejorSolucion());
        sumaEstadistica+=p.mejorValor;
        if (p.mejorValor==86756) optimoCount++;
        it++;
    }
    mejorEver->mostrar();
    std::cout<<((mejorEver->isConexa())?"CONEXA":"NO CONEXA")<<std::endl;    
    std::cout<<"COSTO: "<<mejorEver->evaluar(m)<<std::endl;
    //std::cout<<"PROMEDIO: "<<static_cast<double>(sumaEstadistica)/ITER <<std::endl;
    std::cout<<"OPTIMOS: "<<optimoCount <<std::endl;

    auto fin = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> duracion = fin - inicio;
    std::cout<<"duracion Total: "<<duracion.count()<<std::endl;

//    Tour t (std::vector<int>{0, 21, 30, 17, 2, 16, 20, 41, 6, 1, 29, 22, 19, 49, 28, 15, 45, 43, 33, 34, 35, 38, 39, 36, 37, 47, 23, 4, 14, 5, 3, 24, 11, 27, 26, 25, 46, 12, 13, 51, 10, 50, 32, 42, 9, 8, 7, 40, 18, 44, 31, 48, 0});

    return 0;
}


