#include "poblacion.h"
#include "tour.h"
#include <cassert>
#include <functional>
#include <iostream>
#include <optional>
#include <queue>
#include <random>
#include <thread>
#include <mutex>
#include <format>
#include <utility>
#include <vector>
Poblacion::Poblacion(int n, const Mapa &m):soluciones(n){
    auto inicio = std::chrono::high_resolution_clock::now();
    
    int ind=0;
    std::mutex mtx;
    std::vector<std::thread> hilos;
    int p=n/HILOS;
    int q=n%HILOS;
    for (int i=0;i<HILOS;i++){
        //setea el tamaño de individuos a generar (p o p+1)
        int k=(i<q)?p+1:p;
        hilos.emplace_back([&mtx,this,&ind,&m,k](){
            //por cada hilo establece un generador (no es threadSafe, por lo que no deben compartir)
            std::random_device seeder;
            const auto seed {seeder.entropy() ? seeder() : time(nullptr)};
            std::mt19937 engine { static_cast<std::mt19937::result_type>(seed)};

            for (int j=0;j<k;j++){
                mtx.lock();
                int indice=ind++;
                mtx.unlock();
                soluciones[indice]=crearIndividuo(m, engine);
                assert(soluciones[indice]->isConexa());
            }
        });
    }
    for (auto &hilo:hilos){
        hilo.join();
    }
    //reemplaza un individuo con el mejorAnterior
    std::sort(soluciones.begin(),soluciones.end());
    estadisticas();

    auto fin = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> duracion = fin - inicio;
    std::cout<<"Gen0 duracion: "<<duracion.count()<<std::endl;
    std::cout<<"individuos generados: "<<ind<<std::endl;
}
void Poblacion::reporte() {
    std::cout<<std::format("POB mejor:{}, promedio:{}, peor:{}",mejorValor,promedio,peorValor)<<std::endl;
}

Tour Poblacion::crearIndividuo(const Mapa &m, std::mt19937 &engine) {
    const int MAXITERSINMEJORA=m.data.size();
    //2 opt
    std::optional<Tour> mejor;
    for (int i=0;i<ITERACIONES;i++){
        Tour t{m,engine};
        t.costo=t.evaluar(m);
        
        //busqueda en profundidad con 2-3-opt
        int nSinMejora=0;
        while (nSinMejora<MAXITERSINMEJORA){
            int ganancia=t.twoOpt(engine, m, mejor);
            if (ganancia>0){
                nSinMejora=0;
                t.costo-=ganancia;
            } else {
                nSinMejora++;
            }
        }

        if (!mejor.has_value() || t.costo<mejor->costo){
            mejor=std::move(t);
        }
    }
    return mejor.value();
}

std::pair<Tour *, Tour *> Poblacion::elegirPadres(std::mt19937 &generador)  {
    std::uniform_int_distribution<int> dist(0,soluciones.size()-1);
    std::priority_queue<Tour *,std::vector<Tour *>,std::greater<Tour *>> seleccionados;
    for (int i=0;i<SELECCIONADOS;i++){
        Tour *elegido=&soluciones[dist(generador)].value();
        seleccionados.push(elegido);
    }
    Tour * p=seleccionados.top();
    seleccionados.pop();
    Tour * m=seleccionados.top();
    return std::pair(p,m);
}

void Poblacion::nuevaGeneracion(const Mapa &mapa) {
    double suma=0.0;
    int n=soluciones.size();
    std::mutex mtx;
    std::vector<std::thread> hilos;
    int nHijos=n+static_cast<int>(MORTALIDAD_INFANTIL*n);
    int nPadres=n/200;
    int ind=0;
    std::vector<std::optional<Tour>> nuevaGeneracion(nHijos);
    for (int i=0;i<HILOS;i++){
        hilos.emplace_back([&mtx,this,&ind,&nuevaGeneracion,&mapa,nHijos](){
            std::random_device seeder;
            const auto seed {seeder.entropy() ? seeder() : time(nullptr)};
            std::mt19937 engine{ static_cast<std::mt19937::result_type>(seed)};
            std::uniform_int_distribution<int> probMut(0,99);    

            while (true){
                std::pair<Tour*,Tour*> padres=elegirPadres(engine);
                int comp=padres.first->getCompatibilidad(engine, padres.second);
                int nH=1;
                if (comp>UMBRAL_MAX_AFINIDAD) 
                    nH=3;
                else if (comp>UMBRAL_MEDIA_AFINIDAD)
                    nH=2;

                for (int h=0;h<nH;h++){
                    mtx.lock();
                    int indice=ind++;
                    int mejorV=mejorValor;
                    mtx.unlock();
                    if (indice>=nHijos) {
                        goto fuera;
                    }
                    if (probMut(engine)<50) std::swap(padres.first,padres.second);
                    Tour h1 {mapa,*padres.first,*padres.second,engine,soluciones[0]};
                    h1.costo=h1.evaluar(mapa);
                    if (h1.costo<mejorV){
                        mtx.lock();
                        mejorValor=h1.costo;
                        mtx.unlock();
                    } else if (probMut(engine)<PROBMUT) {
                        h1.costo-=h1.mutar(engine, mapa);
                    }
                    nuevaGeneracion[indice]=std::move(h1);
                }
            }
            fuera:;
        });
    }
    for (auto &hilo:hilos){
        hilo.join();
    }
    //los nuevos pisan los viejos mas malos
    soluciones.resize(nHijos + nPadres);
    int j=nPadres;
    for (int i=0;i<nHijos;i++){
        soluciones[j++]=std::move(nuevaGeneracion[i]);
    }
    
    //se reordena
    std::sort(soluciones.begin(),soluciones.end());
    soluciones.resize(n); //deja a los n mejores
    estadisticas();    
}
void Poblacion::estadisticas() {
    double suma=0.0;
    for (auto &sol:soluciones){
        suma+=sol->costo;
    }
    mejorValor=soluciones[0]->costo;
    peorValor=soluciones[soluciones.size()-1]->costo;
    promedio=suma/soluciones.size();
}
std::optional<Tour> &Poblacion::getMejorSolucion() { return soluciones[0]; }

